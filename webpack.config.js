const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, argv) => ({
  entry: ['./assets/app.scss'],

  output: {
    path: path.join(__dirname, 'build/'),
  },

  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
          presets: [
            ['es2015'],
            ['env', {
              targets: {
                browsers: ['last 2 versions', 'safari >= 7'],
              },
            }],
          ],
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              importLoader: 2,
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.otf?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000',
      },
      {
        test: /\.(svg)(\?[\s\S]+)?$/,
        use: 'file-loader',
      },
      {
          test: /\.(png)$/i,
          use: [
              'file-loader?name=/assets/[name].[ext]',
          ]
      }
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
    }),
    new MiniCssExtractPlugin({
      chunkFilename: '[name].css',
      sourceMap: false,
    }),
  ],
});
